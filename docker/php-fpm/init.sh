#!/usr/bin/env bash

# PHP-FPM container initialisation

function progressbar() {
  echo -e "\n\n\033[1;34m ============== $1% ========================== \033[0;32m\n\n"
}

php -d memory_limit=-1 /usr/bin/composer install -n

progressbar 10

while ! nc -z "${DATABASE_HOST}" "${DATABASE_PORT}" </dev/null; do sleep 1; done

progressbar 20

bin/console doctrine:migrations:migrate -n

progressbar 30

bin/console doctrine:fixtures:load -n

progressbar 40

npm cache clean --force
rm -rf ~/.npm
rm -rf node_modules
rm -f package-lock.json
#npm rebuild node-sass &

progressbar 50

npm install
npm install -g npm

progressbar 60

npm audit fix

progressbar 70

#npm run build

progressbar 80

npm run dev

progressbar 90

bin/console cache:clear
bin/console cache:warmup

progressbar 100

echo -e "\n\n\033[1;34m Open your browser: http://localhost:${NGINX_PORT} \033[0;32m\n\n"
php-fpm
