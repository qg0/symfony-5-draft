# Installation
## Docker installation
Run the docker service
```
service docker start
```
Release the ```80```, ```3306``` and ```9000``` ports or change necessary ports in `.env` file 
```
service nginx stop; service mysql stop; service php-fpm stop; docker stop $(docker ps -a -q)
```
Create the container
```
docker-compose up
```
## Manual installation
### Edit the hosts file ```/etc/hosts```
```
127.0.0.1 mag.loc
```
and invalidate the hosts file
```
nscd -i hosts
```

### Nginx installation
#### Edit the virtual host config, e.g.: ```/etc/nginx/vhosts.d/mag.conf```
```
server {
    server_name localhost;
    root /srv/www/htdocs/mag.loc/public;

    location / {
        try_files $uri /index.php$is_args$args;
    }
      
    location ~.php$ {
        fastcgi_pass 127.0.0.1:65432;
        include fastcgi_params;
        fastcgi_param SCRIPT_FILENAME $realpath_root$fastcgi_script_name;
        fastcgi_param DOCUMENT_ROOT $realpath_root;
        fastcgi_index index.php;
        
        
        keepalive_timeout 0;
        keepalive_requests 0;
                 
        chunked_transfer_encoding on;
    }
}
```
#### Edit the nginx host in ```.env``` file

```
NGINX_SCHEME=http
NGINX_HOST=localhost
NGINX_PORT=80

PHPFPM_HOST=localhost
PHPFPM_PORT=9000

```

### Install the packages from the `composer.json` file
```
composer install
```
### Database creation

#### Add the database credentials to the `.env` file
```
DATABASE_DRIVER=mysql
DATABASE_USER=root
DATABASE_PASSWORD=password
DATABASE_HOST=localhost
DATABASE_PORT=3306
DATABASE_NAME=magdev
```

#### Create the database

```
bin/console doctrine:databae:create 
```

#### Apply the migrations
```
bin/console doctrine:migrations:migrate
```

#### Apply the fixtures
```
bin/console doctrine:fixtures:load
```

### Frontend installation
#### Rebuild node-sass for your architecture
```
npm rebuild node-sass
```
#### Install npm packages
```
npm install
```

#### Apply security patches
```
npm audit fix
```

#### Production build
```
npm run build
```

#### Development build
```
npm run watch
```

# Quality control
## Coding standard

### Using the PHP Coding Standards Fixer
```
vendor/bin/php-cs-fixer fix src
vendor/bin/php-cs-fixer fix tests
```

### Using the PHP CodeSniffer

#### Install the Symfony coding standard
```
vendor/bin/phpcs --config-set installed_paths vendor/escapestudios/symfony2-coding-standard
```

#### Run the code sniffer
```
vendor/bin/phpcs --standard=Symfony src
vendor/bin/phpcs --standard=Symfony tests
```

## Testing

## Unit testing
```
phpunit
```

## Unit testing in the docker container
```
docker exec -it php-fpm phpunit
```

### Output
```

PHPUnit 9.2.3 by Sebastian Bergmann and contributors.

Testing 
..........................................                        42 / 42 (100%)

Time: 01:12.849, Memory: 52.50 MB

OK (42 tests, 4675 assertions)

```

## Stress testing

### Stress testing using the ApacheBench
```
ab -c 100 -n 10000 http://mag.loc/api/v1/document 
```
#### Output
```
Completed 10000 requests
Finished 10000 requests


Server Software:        nginx/1.19.0
Server Hostname:        mag.loc
Server Port:            80

Document Path:          /api/v1/document
Document Length:        595 bytes

Concurrency Level:      100
Time taken for tests:   601.370 seconds
Complete requests:      10000
Failed requests:        0
Total transferred:      9580000 bytes
HTML transferred:       5950000 bytes
Requests per second:    16.63 [#/sec] (mean)
Time per request:       6013.698 [ms] (mean)
Time per request:       60.137 [ms] (mean, across all concurrent requests)
Transfer rate:          15.56 [Kbytes/sec] received

Connection Times (ms)
              min  mean[+/-sd] median   max
Connect:        0    0   2.2      0      27
Processing:    76 5981 995.1   5579    9562
Waiting:       63 5981 995.2   5579    9562
Total:         77 5981 994.5   5579    9562

Percentage of the requests served within a certain time (ms)
  50%   5579
  66%   5827
  75%   6595
  80%   6803
  90%   7381
  95%   7767
  98%   8915
  99%   9215
 100%   9562 (longest request)

```

# Transactions and Concurrency
[The Doctrine library has the mechanism to solve this problems.](https://www.doctrine-project.org/projects/doctrine-orm/en/latest/reference/transactions-and-concurrency.html)
## Transactions
The approach is to use the implicit transaction handling provided by the Doctrine ORM EntityManager. Given the following code snippet, without any explicit transaction demarcation:
```
<?php
// $em instanceof EntityManager
$user = new User;
$user->setName('George');
$em->persist($user);
$em->flush();

```
## Optimistic Locking

Database transactions are fine for concurrency control during a single request. However, a database transaction should not span across requests, the so-called "user think time". Therefore a long-running "business transaction" that spans multiple requests needs to involve several database transactions. Thus, database transactions alone can no longer control concurrency during such a long-running business transaction. Concurrency control becomes the partial responsibility of the application itself.

Doctrine has integrated support for automatic optimistic locking via a version field. In this approach any entity that should be protected against concurrent modifications during long-running business transactions gets a version field that is either a simple number (mapping type: integer) or a timestamp (mapping type: datetime). When changes to such an entity are persisted at the end of a long-running conversation the version of the entity is compared to the version in the database and if they don't match, an OptimisticLockException is thrown, indicating that the entity has been modified by someone else already.

```
try {
    $entity = $em->find('User', $theEntityId, LockMode::OPTIMISTIC, $expectedVersion);

    // do the work

    $em->flush();
} catch(OptimisticLockException $e) {
    echo "Sorry, but someone else has already changed this entity. Please apply the changes again!";
}
```
